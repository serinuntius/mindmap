import java.awt.*;
import javax.swing.*;
PImage webImg =null;
int imgX,imgY;
boolean newcreate = false;
boolean addcreate = false;

class Node{
    String str;
    int x,y;
    int nWidth,nHeight=30;
    int px,py;
    //新規作成のとき
    Node(String _str,int _x,int _y){
        str = _str;
        x = _x;
        y = _y;
        nWidth = int(textWidth(str));
        px = x+nWidth/2;
        py = y+nHeight/2;
    }
    //追加作成のとき
    Node(String _str,int _x,int _y,int num){
        str = _str;
        x = _x;
        y = _y;
        nWidth = int(textWidth(str));
        px = n.get(num).x; 
        py = n.get(num).y;
    }

    void display(){
        fill(0);
        line(x+nWidth/2, y+nHeight/2, px, py);
        fill(255);
        rect(x,y,nWidth*2+8+8,nHeight);

        fill(0,0,0);
        textAlign(LEFT,CENTER);
        
        text(str,(x*2+nWidth)/2,(y*2+nHeight)/2);
    
    }

}



ArrayList<Node> n;

JPanel panel = new JPanel();
JTextField text1;

void setup(){
    size(512,512);
    n =  new ArrayList<Node>();

    BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
    panel.setLayout(layout);

    panel.add(new JLabel("入力"));
    text1 = new JTextField();
    panel.add(text1);
}

void draw(){
    background(255);
    for (int i = 0; i < n.size(); ++i) {
        n.get(i).display();
    }/*
  if(webImg != null){
    webImg.resize(webImg.width/10,webImg.height/10);
    image(webImg,imgX,imgY);
    println("img!");  

  }*/
  

}
 
void mousePressed(){
    if(mouseButton == RIGHT){
        if(newcreate){
            n.add(new Node(input(),mouseX,mouseY));
        }
        else if(addcreate){
            n.add(new Node(input(),mouseX,mouseY,n.size()-1));
        }

    }

}

void keyPressed(){
    if(key == 'i'){
        String url = input();
        println(url);
        String str[] = loadStrings("https://www.googleapis.com/customsearch/v1?key=AIzaSyCDIu0aU964IOLmx8MACeLpjXwTgf2ZwcQ&cx=000893818175528849387:9ml_7eulfck&searchType=image&q="+url);
        imgX=mouseX;
        imgY=mouseY;
  
  
        for(int i = 0;i<str.length;i++){
            if(str[i].indexOf("\"link\"") != -1){
                println(str[i]);
                println();
                String tmp = str[i].substring(str[i].indexOf("h"));
                println("OK!　　　　　　"+tmp.substring(0,tmp.length()-2));
                webImg = loadImage(tmp.substring(0,tmp.length()-2));
                break;
            }
        }
    }
    //新規作成モード
    if(key == 'n'){
        newcreate = true;
        addcreate = false;
    }
    //追加作成モード
    if(key == 'a'){
        addcreate = true;
        newcreate = false;
    }



}  
  
String input(){
	JOptionPane.showConfirmDialog(
	null,   // オーナーウィンドウ
	panel,   // メッセージ
	"日本語入力",   // ウィンドウタイトル
	JOptionPane.OK_CANCEL_OPTION,  // オプション（ボタンの種類）
	JOptionPane.QUESTION_MESSAGE);  // メッセージタイプ（アイコンの種類）
	return text1.getText();
}

